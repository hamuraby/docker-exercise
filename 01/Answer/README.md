# Description
El ejercicio tiene un script de NodeJS que se conecta a un MongoDB y se accede a través de una interfaz ReST en el puerto 3000.
Los tests de regresión se realizan una colección de Postman que se ejecuta mediante Newman.

El siguiente script instalará las dependencias de la aplicación en el volumen indicado en el contenedor:

    docker run -it --rm -w /work -v $(pwd):/work node:6.9.1 npm install

Este script facilita la ejecución:

    ./regrtest-compose.sh

Una vez levantada la web se puede comprobar su funcionamiento a través de curl o del navegador:

    curl http://127.0.0.1:3000


## Run instructions

Install the necessary node modules like this

    docker run -it --rm -w /work -v $(pwd):/work node:6.9.1 npm install

First, start the db and web services in the background

    docker-compose -f docker-compose.yml up -d  db web

Next, run the regression test service in the forground

    docker-compose -f docker-compose.yml up test

When the regression tests complete then stop the environment

    docker-compose -f docker-compose.yml down

