# Description
Este es un ejercicio en el que se va a utilizar un script NodeJS existente que se conecta a una base de datos Mongo.
El script Node tiene un servicio ReST en el puerto 3000.

El objetivo es dockerizar esta aplicación, pero sin crear una imagen para ello. En su lugar, lo que hay que hacer es ejecutar los contenedores adecuados para ello.
Sin embargo, al no crear una imagen significa que la fuente del script y el directorio node_modules asociado deben pasarse del sistema de archivos del host al sistema de archivos del contenedor.

docker-compose se puede usar para iniciar contenedores sin necesidad de un Dockerfile para compilarlos.
Los servicios Mongo (db), Node (web) y curl (prueba) se pueden definir en el archivo docker-compose.yml.

La prueba de regresión se realiza utilizando curl para acceder a la interfaz ReST en el script del nodo.

Esto es un ejemplo del uso de herramientas como Mongo y NodeJS sin instalarlas ejecutándolas como contenedores.

Es necesario utilizar imágenes para NodeJS versión 6.9.1 y MongoDB versión 3.

El objetivo es crear un archivo docker-compose.yml que se pueda usar para iniciar la base de datos y los servicios web y también probar el servicio web con un simple comando curl para demostrar que se puede alcanzar el end-point.
Un desafío a tener en cuenta es que los contenedores deben iniciarse de una manera que permita que el script del nodo se conecte a la base de datos de Mongo y luego permita que curl se use para acceder a la interfaz ReST.

El servicio se puede consumir con curl y devolverá un "Hello World".

Como punto de partida, las instrucciones de ejecución le mostrarán cómo utilizar un contenedor de nodos para instalar los módulos necesarios (contenidos en el archivo package.json).

## Run instructions

Para instalar los módulos necesarios:

    docker run -it --rm -w /work -v $(pwd):/work node:6.9.1 npm install
